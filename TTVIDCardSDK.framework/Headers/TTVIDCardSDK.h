//
//  TTVIDCardSDK.h
//  TTVIDCardSDK
//
//  Created by user on 4/9/22.
//

#import <Foundation/Foundation.h>

//! Project version number for TTVIDCardSDK.
FOUNDATION_EXPORT double TTVIDCardSDKVersionNumber;

//! Project version string for TTVIDCardSDK.
FOUNDATION_EXPORT const unsigned char TTVIDCardSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TTVIDCardSDK/PublicHeader.h>


#import "TTVIDCardEngine.h"
